package com.example.Viewer;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


import java.io.File;
import java.text.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.zip.Inflater;


public class ThumbViewerWithDate extends Activity {
    ListView mListViewMain;

    LinkedList<ListsByDate> mDateList;
    int mColPerRow=3;
    int mCellSize=150;
    int mRealCellSize=0;
    MainListAdapter mAdapter ;
    boolean hadUpdateImage=false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thumb_viewer_with_date);
        mListViewMain = (ListView)findViewById(R.id.listViewMain) ;

        mDateList =new LinkedList<ListsByDate>() ;




        mListViewMain.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            reCaculateRow(right-left);
            if (!hadUpdateImage) {
                hadUpdateImage=true;
                updateImages();
            }
            }
        });
    }

    void reCaculateRow(int width) {
        mColPerRow = width/mCellSize ;
        mRealCellSize = width/mColPerRow ;
        Log.d("Recaculate Row","colum per row : " + mColPerRow + "  Cell size : "+mRealCellSize + " view size: "+width) ;
    }
    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * A image name collection which capture date is same
     */
    class ListsByDate implements Comparable<ListsByDate> {
        long  date_inMilliseconds ;
        List<ImageFileInfo> imagesName;
        ListsByDate(long milliseconds){
            date_inMilliseconds = milliseconds;
            imagesName = new ArrayList<ImageFileInfo>();
        }
        void addImage(ImageFileInfo imgInfo) {
            imagesName.add(0,imgInfo);
        }
        ImageFileInfo getImageInfo(int position) {
            return imagesName.get(position);
        }
        int numberOfImages() {
            return imagesName.size() ;
        }
        boolean isContainImage(ImageFileInfo newImageInfo) {
            if (imagesName==null) return false;
            for (ImageFileInfo image:imagesName) {
                if (image.getFullPathName().equals(newImageInfo.getFullPathName())) return true;
            }
            return false;
        }
        long getDate() { return date_inMilliseconds;}

        /**
         * Compares this object to the specified object to determine their relative
         * order.
         *
         * @param another the object to compare to this instance.
         * @return a negative integer if this instance is less than {@code another};
         * a positive integer if this instance is greater than
         * {@code another}; 0 if this instance has the same order as
         * {@code another}.
         * @throws ClassCastException if {@code another} cannot be converted into something
         *                            comparable to {@code this} instance.
         */
        @Override
        public int compareTo(ListsByDate another) {
            return (int)(date_inMilliseconds - another.date_inMilliseconds);
        }
    }



//    class CellItemView extends RelativeLayout {
//
//
//    }
    class ListItemDateView extends LinearLayout {
        ListsByDate mList;
        TextView txtDate;
        public ListItemDateView(Context context) {
            super(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_item_date_view, this);
            txtDate = (TextView)view.findViewById(R.id.txtDate);
        }
        ListsByDate getLists() { return mList;}
        void setLists(ListsByDate list) {
            mList=list;
            DateFormat formater = SimpleDateFormat.getDateInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(mList.getDate());

            txtDate.setText(formater.format(calendar.getTime()));
        }
    }

    class ListRowView extends LinearLayout {
        Context mContext;
        int numberOfColumns;
        CellItemView[] childView;
        public ListRowView(Context context) {
            super(context);
            mContext=context ;
            numberOfColumns=mColPerRow;
            childView = new CellItemView[numberOfColumns];
            initialColumns();
        }
        private void initialColumns() {
            setOrientation(LinearLayout.HORIZONTAL);
            setGravity(Gravity.CENTER_HORIZONTAL);
            setPadding(0, 0, 0, 0);

            for (int i=0;i<numberOfColumns;i++) {

                childView[i]=new CellItemView(mContext) ;
                childView[i].setLayoutParams(new LinearLayout.LayoutParams(mRealCellSize,mRealCellSize));
                addView(childView[i]);
                childView[i].setVisibility(View.INVISIBLE);
            }
        }
        public void setItems(ImageFileInfo[] images) {
            int index =0;
            for (ImageFileInfo item:images) {
                if (item==null) {

                    break;
                }
                childView[index].setImageInfo(item);
//                childView[index].setVisibility(View.VISIBLE);
                index++;
            }
            for (;index<numberOfColumns;index++) {
                childView[index].setImageInfo(null);
//                childView[index].setVisibility(View.INVISIBLE);
            }

        }
    }
    ListsByDate findDateLists(long date) {
        for (ListsByDate dateItem : mDateList)
        {
            if (dateItem.getDate()==date ) return dateItem;
        }
        return null ;
    }

    class MainListAdapter extends BaseAdapter {

        ListItemDateView getListItemDateView() {
            return new ListItemDateView(ThumbViewerWithDate.this) ;
        }

        ListRowView getListRowView() {
            return  new ListRowView(ThumbViewerWithDate.this) ;

        }
        /**
         * How many items are in the data set represented by this Adapter.
         *
         * @return Count of items.
         */
        @Override
        public int getCount() {
            int count = mDateList.size();
            for (ListsByDate dateItem : mDateList)
            {
                count += ( (dateItem.numberOfImages()+mColPerRow-1)/mColPerRow);
            }

            return count ;
        }

        /**
         * Get the data item associated with the specified position in the data set.
         *
         * @param position Position of the item whose data we want within the adapter's
         *                 data set.
         * @return The data at the specified position.
         */
        @Override
        public Object getItem(int position) {
            int index =0,numberOfRow,count;
            for (ListsByDate dateItem : mDateList)
            {
                if (index==position) return dateItem;
                index++;
                count = dateItem.numberOfImages();
                numberOfRow = ( (count+mColPerRow-1)/mColPerRow) ;
                if ( (position - index) <  numberOfRow) {
                    ImageFileInfo images[] = new ImageFileInfo[mColPerRow];
                    int startIndex = (position-index)*mColPerRow;
                    for (int i= 0; i<mColPerRow ;i++ ) {
                        images[i] = ((startIndex + i) < count) ? dateItem.getImageInfo(startIndex + i) : null;
                    }
                    return images ;
                }
                index+= numberOfRow ;
            }
            return null;
        }

        @Override
        public int getItemViewType(int position) {
            int index =0,numberOfRow,count;
            for (ListsByDate dateItem : mDateList) {
                if (index == position) return 0;
                index++;
                count = dateItem.numberOfImages();
                numberOfRow = ( (count+mColPerRow-1)/mColPerRow) ;
                if ( (position - index) <  numberOfRow) return 1;
                index += numberOfRow;
            }
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        /**
         * Get the row id associated with the specified position in the list.
         *
         * @param position The position of the item within the adapter's data set whose row id we want.
         * @return The id of the item at the specified position.
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * Get a View that displays the data at the specified position in the data set. You can either
         * create a View manually or inflate it from an XML layout file. When the View is inflated, the
         * parent View (GridView, ListView...) will apply default layout parameters unless you use
         * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
         * to specify a root view and to prevent attachment to the root.
         *
         * @param position    The position of the item within the adapter's data set of the item whose view
         *                    we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view
         *                    is non-null and of an appropriate type before using. If it is not possible to convert
         *                    this view to display the correct data, this method can create a new view.
         *
         *                    Heterogeneous lists can specify their number of view types, so that this View is
         *                    always of the right type (see {@link #getViewTypeCount()} and
         *                    {@link #getItemViewType(int)}).
         * @param parent      The parent that this view will eventually be attached to
         * @return A View corresponding to the data at the specified position.
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Object item = getItem(position) ;
            boolean needCreateView=true;
            if (item instanceof ImageFileInfo[]) {
                if (convertView != null) {
                    if (!(convertView instanceof ListRowView)) {
                        convertView = null;
                    }
                }
                if (convertView == null) {
                    convertView = getListRowView() ; //new ListRowView(ThumbViewerWithDate.this);
                }
                ((ListRowView) convertView).setItems((ImageFileInfo[]) item);
            } else if (item instanceof ListsByDate) {
                if (convertView != null) {
                    if (!(convertView instanceof ListItemDateView)) {
                        convertView = null;
                    }
                }
                if (convertView == null) {
                    convertView = getListItemDateView();   //new ListItemDateView(ThumbViewerWithDate.this);
                }
                ((ListItemDateView) convertView).setLists((ListsByDate) item);
            }
            else convertView=null ;

            return convertView;
        }
    }

    /**
     * Invoke AsyncTask to get images information
     */
    private void updateImages() {

        new AsyncTask<Void,Integer,Void>() {
            Uri uri  = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String[] projection = new String[] {
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATE_TAKEN,
                    MediaStore.Images.Media.DISPLAY_NAME,
                    MediaStore.Images.Media.DATA,                               // file path
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.ImageColumns._ID
            };
            /**
             * Override this method to perform a computation on a background thread. The
             * specified parameters are the parameters passed to {@link #execute}
             * by the caller of this task.
             * <p/>
             * This method can call {@link #publishProgress} to publish updates
             * on the UI thread.
             *
             * @param params The parameters of the task.
             * @ = return A result, defined by the subclass of this task.
             * @see #onPreExecute()
             * @see #onPostExecute
             * @see #publishProgress
             */
            @Override
            protected Void doInBackground(Void... params) {
                long imageDate;
                String imageName;
                String imagePath;
                String folderName;
                ListsByDate nameList;
                int thumbId;
                Cursor cursor = ThumbViewerWithDate.this.getContentResolver().query(
                        uri,projection,null,null,MediaStore.Images.Media.DATE_TAKEN
                );
                if (cursor.moveToFirst()) {
                    int dateColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
                    int nameColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                    int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    int bucketColumnIndex=cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    int thumbIdColumnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    ImageFileInfo image;
                    do {
                        imageDate = cursor.getLong(dateColumnIndex);
                        imageName = cursor.getString(nameColumnIndex);
                        imagePath = cursor.getString(dataColumnIndex);
                        folderName = cursor.getString(bucketColumnIndex);

                        image = new ImageFileInfo(imageName,imagePath,imageDate,cursor.getLong(thumbIdColumnIndex)) ;
//                        Log.d("image"," Date:"+image.getTakeDateInString(ImageFileInfo.TAKE_DATE_TRUNK_TO_DATE)+"  name:"+imageName);
                        long folderDate= image.getTakeDateInMilliseconds(ImageFileInfo.TAKE_DATE_TRUNK_TO_DATE);
                        nameList = findDateLists(folderDate) ;
                        if (nameList==null) {
                            nameList = new ListsByDate(folderDate);
                            mDateList.add(0,nameList) ;
                        }
                        if (nameList!=null) {
                            if (!nameList.isContainImage(image)) nameList.addImage(image);
                        }
                    }while(cursor.moveToNext()) ;
                    // do not need sort here, because we query sort with DATE
                }
                cursor.close();
                return null;
            }

            /**
             * Runs on the UI thread after {@link #publishProgress} is invoked.
             * The specified values are the values passed to {@link #publishProgress}.
             *
             * @param values The values indicating progress.
             * @see #publishProgress
             * @see #doInBackground
             */
            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
            }

            /**
             * <p>Runs on the UI thread after {@link #doInBackground}. The
             * specified result is the value returned by {@link #doInBackground}.</p>
             * <p/>
             * <p>This method won't be invoked if the task was cancelled.</p>
             *
             * @param aVoid The result of the operation computed by {@link #doInBackground}.
             * @see #onPreExecute
             * @see #doInBackground
             * @see #onCancelled(Object)
             */
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mAdapter = new MainListAdapter();
                mListViewMain.setAdapter(mAdapter);
//                for (ListsByDate item:mDateList) {
//                    Log.d("dateList:"," date:"+item.getDate() + " image numbers:"+item.numberOfImages());
//                }
//                mAdapter.notifyDataSetChanged();
            }
        }.execute() ;
    }
}