package com.example.Viewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.util.concurrent.LinkedBlockingDeque;


public class CellItemView extends LinearLayout {

    static LinkedBlockingDeque<CellItemView> mWaitForLoadImageCellItem = null ; //new LinkedBlockingDeque<CellItemView>() ;

    Context mContext;
    ImageFileInfo mImage;
    Bitmap mBitmap;
    CheckBox mSelectCheckBox;
    ImageView mImageView;

    class LoadImageAsyncTask extends AsyncTask<Void,CellItemView,Void>
    {

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(Void... params) {
            CellItemView item;
            while(!isCancelled()) {
                synchronized (mWaitForLoadImageCellItem) {
                    item = mWaitForLoadImageCellItem.poll();
                }
                if (item!=null) {
                    synchronized (item) {
                        Bitmap origBmp = item.mBitmap;

                        item.mBitmap = item.getThumbnail();
                        publishProgress(item);
// TODO , hope to recycle it . But it will cause error sometimes                       if (origBmp != null) origBmp.recycle();
                    }
                }
            }
            return null;
        }

        /**
         * Runs on the UI thread after {@link #publishProgress} is invoked.
         * The specified values are the values passed to {@link #publishProgress}.
         *
         * @param values The values indicating progress.
         * @see #publishProgress
         * @see #doInBackground
         */
        @Override
        protected void onProgressUpdate(CellItemView... values) {
            super.onProgressUpdate(values);
            values[0].mImageView.setImageBitmap(values[0].mBitmap);
        }
    }
//    static Runnable mLoadImageRunnable =new Runnable() {
//
//        /**
//         * Starts executing the active part of the class' code. This method is
//         * called when a thread is started that has been created with a class which
//         * implements {@code Runnable}.
//         */
//        @Override
//        public void run() {
//            CellItemView item;
//            while(true) {
//                item =(CellItemView)mWaitForLoadImageCellItem.poll();
//                if (item!=null) {
//                    Bitmap origBmp=item.mBitmap;
//
//                    item.mBitmap=item.getThumbnail();
//
//                    item.mImageView.setImageBitmap(item.mBitmap);
//                    if (origBmp!=null) origBmp.recycle();
//                }
//            }
//        }
//    };
    static void goWaitLoadImage(CellItemView cell){
        try {
            synchronized (mWaitForLoadImageCellItem) {
                mWaitForLoadImageCellItem.put(cell);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    static void cancelLoadImage(CellItemView cell) {
        synchronized (mWaitForLoadImageCellItem) {
            if (mWaitForLoadImageCellItem.contains(cell)) {
                mWaitForLoadImageCellItem.remove(cell);
            }
        }
    }
    public CellItemView(Context context) {
        super(context);
        mContext=context ;

        if (mWaitForLoadImageCellItem==null) {
            mWaitForLoadImageCellItem=new LinkedBlockingDeque<CellItemView>() ;
            new LoadImageAsyncTask().execute();
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item_cell_view, this);
        mSelectCheckBox = (CheckBox)findViewById(R.id.cellCheckBox);
        mImageView = (ImageView)findViewById(R.id.cellImageView);
        mBitmap=null;

    }
    private Uri getPathUri() {
        return Uri.fromFile(new File(mImage.getFullPathName()));
    }
    private Bitmap getThumbnail() {
//            Log.d("GetThumb", "Thumb id:" + mImage.getThumbId());
        if (mImage==null) return null;
        return MediaStore.Images.Thumbnails.getThumbnail(
                mContext.getContentResolver(),
                mImage.getThumbId(),
                MediaStore.Images.Thumbnails.MINI_KIND,
                null );
    }
    public void setImageInfo(ImageFileInfo image) {
        synchronized (this) {
            cancelLoadImage(this);
            mImage = image;
            setVisibility(mImage == null ? View.INVISIBLE : View.VISIBLE);
            if (mImage != null) goWaitLoadImage(this);
            mImageView.setImageBitmap(null);
        }
//        Bitmap origBmp=mBitmap;
//
//        mBitmap=getThumbnail();
//        mImageView.setImageBitmap(mBitmap);
//        if (origBmp!=null) origBmp.recycle();
    }

}
