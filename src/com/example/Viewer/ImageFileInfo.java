package com.example.Viewer;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ImageFileInfo {
    long mTakeDate;
    String mFilePath;
    String mDisplayName;
    long mThumbId;
    public ImageFileInfo(String displayName,String filePath,long takeDate,long thumbId) {
        mTakeDate=takeDate;
        mFilePath=filePath;
        mDisplayName=displayName;
        mThumbId=thumbId;
    }
    public String getFullPathName() { return mFilePath; }
    public String getDisplayName() { return mDisplayName; }
    public long getTakeDateInMilliSeconds() { return mTakeDate; }
    public long getThumbId() { return mThumbId; }
    public final static int TAKE_DATE_TRUNCAT_NO=0;
    public final static int TAKE_DATE_TRUNK_TO_DATE=8;
    public final static int TAKE_DATE_TRUNK_TO_MONTH=16 | TAKE_DATE_TRUNK_TO_DATE;
    public final static int TAKE_DATE_TRUNK_TO_YEAR=32 | TAKE_DATE_TRUNK_TO_MONTH;

    private Calendar getTruncatDate(int dateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mTakeDate);
        if (dateFormat!=TAKE_DATE_TRUNCAT_NO) {
            if ((dateFormat&TAKE_DATE_TRUNK_TO_DATE)!=0) {
                calendar.set(Calendar.MILLISECOND,0);           //**  can not use clear instead of set
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MINUTE,0);
                calendar.set(Calendar.HOUR_OF_DAY,0);
                dateFormat&=~TAKE_DATE_TRUNK_TO_DATE;
            }
            if ((dateFormat&TAKE_DATE_TRUNK_TO_MONTH)!=0) {
                calendar.clear(Calendar.DATE);
                dateFormat&=~TAKE_DATE_TRUNK_TO_MONTH;
            }
            if ((dateFormat&TAKE_DATE_TRUNK_TO_YEAR)!=0) {
                calendar.clear(Calendar.YEAR);
            }
        }
//        Log.d("calendar", " calendar date:" + calendar.getTime());

        return calendar ;
    }
    public long getTakeDateInMilliseconds(int dateFormat)  {

        return getTruncatDate(dateFormat).getTimeInMillis();
    }
    public String getTakeDateInString(int dateFormat)  {

        DateFormat formater;
        switch(dateFormat) {
            case TAKE_DATE_TRUNK_TO_DATE:
                formater = new SimpleDateFormat("yyyy-MM-dd") ;
                break;
            case TAKE_DATE_TRUNK_TO_MONTH:
                formater = new SimpleDateFormat("yyyy-MM") ;
                break;
            case TAKE_DATE_TRUNK_TO_YEAR:
                formater = new SimpleDateFormat("yyyy") ;
                break;
            default:
                formater = SimpleDateFormat.getDateTimeInstance();
                break;
        }
        return formater.format(getTruncatDate(dateFormat).getTime());
    }
}
